## 2.2. Текущий контекст - WebAccess.current

current позволяет нам работать с текущим открытым объектом: получать сведения о нем, 
модифицировать его представление. 

1. Получить информацию об объекте:

    WebAccess.current.getID() -  получит id текущей открытой папки;

2. Изменить кнопки, закладки, ссылки, реквизиты, таблицы, списки:
    
    WebAccess.current.toolBar.buttons['CreateDocument'].hide();
    WebAccess.current.sideBar.tabs['Run'].hide();
    WebAccess.current.form.controls.links['showPhoto'].hide();
    WebAccess.current.form.controls.requisites['Дата'].hide();
    WebAccess.current.form.controls.tables['STXGrid1'].hide();
    WebAccess.current.objectList.getItems();

3. Сохранить карточку текущего объекта:

    WebAccess.current.form.save();

У WebAccess.current есть алиас, вы можете использовать краткую запись WA.CR если хотите;
Скорее всего WA.CR - это первое с чем вам придется поработать вплотную когда вы займетесь 
разработкой веб-модулей. 

Запомните, WebAccess.current может быть использован только после того,
как КОМ инициализировалась, иначе ждите беды. Как дождаться инициализации КОМ - 
рассказано в предыдущих главах.

### Получение информации об объекте

### Работа с кнопками на панели инструменты

### Работа с закладками

### Работа с ссылками

### Работа с реквизитами карточки

### Работа с таблицами

### Работа со списками объектов
