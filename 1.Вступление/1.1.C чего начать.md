# 1. Основы

## 1.1 С чего начать

### Введение

Клиентская объектная модель - это набор инструментов представленных в виде 
JavaScript объектов и их методов, предназначенных для создания модулей к 
веб-доступу DIRECTUM. В данном случае, слово "клиентская" означает, что все 
вычисления происходят на стороне браузера пользователя и реализованы на языке 
JavaScript. Именно поэтому в данной книге речь будет идти исключительно о 
JavaScript и работа серверной части будет затронута лишь для представления общей 
картины.

### Начальные требования

Когда речь заходит об использовании клиентской объектной модели (дальше по 
тексту я буду сокращать это понятие до абревиатуры КОМ), скорее всего это 
означает, что началась разработка нового модуля для веб-доступа к DIRECTUM. 
Однако, КОМ используется и самим веб-доступом для реализации своего базового 
функционала.

Исторически так сложилось что КОМ создавалась для веб-доступа, и работает 
только в нем(но мы планируем выделить портируемую версию, которой возможно 
будет пользоваться и в других продуктах). 

Исходя из этого появляется первое и основное требование для начала работы: вам необходим веб-доступ. Но он не обязательно должен быть доступен в 
виде сервера, работать с КОМ можно и в браузере, и это, кстати, второе 
требование: вам необходим браузер, и лучше если это будет не Internet Explorer.

Хорошо если у вас есть доступ к серверу веб-доступа, тогда вы сможете не 
только познакомиться с КОМ но и использовать её по прямому назначению - для 
создания веб-модулей.

### Первое знакомство

Считаем что первые 2 условия выполнены, и у вас есть браузер, в котором 
открыт веб-доступ. Очень хорошо, если вы уже работали с JavaScript консолью разработки 
которую предоставляет любой современный веб-браузер, иначе будет знакомится с 
ним походу повествования.

Итак, веб-доступ открыт. Теперь откройте консоль разработки - обычно она 
открывается при нажатии на кнопку F12. Если вы увидели поле для ввода - это 
замечательно, именно с ним нам предстоит поработать больше всего. Попробуйте 
ввести туда следующую строку:

	WebAccess.getLocation()

Если в ответ вы увидите что-то похожее на строку "reference" или "login" значит 
вы все делаете правильно, и только что вы выполнили один из методов клиентской объектной 
модели. Этот метод нам еще не раз пригодится, но пока просто знайте, что 
такой метод есть и он возвращает строку с именем страницы на которой мы 
находимся в данный момент.
